/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;


import Interface.IMatriz2;
import Negocio.*;
import java.util.Scanner;
/**
 **
 * @author joseb
 */
public class PruebaMatrizNuevosMetodos {
    
    public static void main(String[] args) throws Exception {
        
        System.out.println("POR FAVOR DIGITE LOS DATOS DE LA MATRIZ");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
            MatrizEntero_Aleatoria myMatriz = new MatrizEntero_Aleatoria(filas, cols);
            myMatriz.crearMatriz(ini, fin);
            System.out.println("Mi matriz:"+myMatriz.toString());
           
            
            System.out.println("Mi matriz:"+myMatriz.toString());
            // Forma1:
            System.out.println("El dato que más se repite es él:"+myMatriz.getMas_Se_Repite());
           
            //Forma 2:POLIMORFISMO
            IMatriz2 i_matriz=myMatriz;
            System.out.println("Total de la forma2:"+i_matriz.getMas_Se_Repite());
            
            //Forma 3:CASTING DE LA INTERFACE
            int total=((IMatriz2)myMatriz).getMas_Se_Repite();
            System.out.println("Total de la forma3:"+total);
            
            
         
            
            
        
    }
  }



